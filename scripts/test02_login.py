# _*_ coding: UTF-8 _*_
# 开发者：朱炳安
# 开发人员：ZHUBINGAN
# 开发时间：2021/9/24 22:53
# 文件名称：test01_login.PY
# 开发工具：PyCharm

import unittest

import config
from api.login import LoginIHRM
from parameterized import parameterized
import json


def build_data():
    test_data = []
    file = "../data/login.json"
    with open(file, encoding="utf-8") as f:
        json_data = json.load(f)
        for case_data in json_data:
            login_data = case_data.get("login_data")
            status_code = case_data.get("status")
            success = case_data.get("success")
            code = case_data.get("code")
            message = case_data.get("message")
            test_data.append((login_data, status_code, success, code, message))
    return test_data


class TestLogin(unittest.TestCase):
    def setUp(self):
        self.login_api = LoginIHRM()

    @parameterized.expand(build_data())
    def test01_login(self, login_data, status_code, success, code, message):
        response = self.login_api.login(login_data)
        print(response.json())
        self.assertEqual(status_code, response.status_code)
        self.assertEqual(success, response.json().get("success"))
        self.assertEqual(code, response.json().get("code"))
        self.assertIn(message, response.json().get("message"))
