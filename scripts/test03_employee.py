# _*_ coding: UTF-8 _*_
# 开发者：朱炳安
# 开发人员：ZHUBINGAN
# 开发时间：2021/9/25 20:52
# 文件名称：test02_employee.PY
# 开发工具：PyCharm

import unittest
from api.employee import EmployeeManage
from utils import common_assert


class TestEmployee(unittest.TestCase):
    # employee_id = 1442057307096612864
    employee_id = None
    # 前置处理
    def setUp(self):
        self.employee_api = EmployeeManage()

    def test01_add_employee(self):
        add_employee_data = {
            "username": "多可12",
            "mobile": "15877897379",
            "workNumber": "tecent0018",
            "timeOfEntry": "2021-02-23",
            "formOfEmployment": 1,
            "departmentId": "98",
            "departmentName": "人力资源部",
            "correctionTime": "2021-05-23"
        }

        # 获取响应结果
        response = self.employee_api.add_employee(add_employee_data=add_employee_data)

        # 断言
        common_assert(self, response, 200, True, 10000, "操作成功")

        # 提取员工ID
        TestEmployee.employee_id = response.json().get("data").get("id")
        print(TestEmployee.employee_id)

    # 修改员工
    def test02_update_employee(self):
        update_employee_data = {"username": "马龙67"}
        response = self.employee_api.update_employee(TestEmployee.employee_id, update_data=update_employee_data)
        print(response.json())

        # 断言
        common_assert(self, response, 200, True, 10000, "操作成功")

    # 查询接口
    def test03_get_employee(self):
        response = self.employee_api.get_employee(TestEmployee.employee_id)
        print(response.json())

        # 断言
        common_assert(self, response, 200, True, 10000, "操作成功")

    # 删除员工
    def test04_delete_employee(self):
        response = self.employee_api.del_employee(TestEmployee.employee_id)
        print(response.json())

        # 断言
        common_assert(self, response, 200, True, 10000, "操作成功")