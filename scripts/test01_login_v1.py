# _*_ coding: UTF-8 _*_
# 开发者：朱炳安
# 开发人员：ZHUBINGAN
# 开发时间：2021/9/24 22:53
# 文件名称：test01_login.PY
# 开发工具：PyCharm

import unittest

import config
from api.login import LoginIHRM
from utils import common_assert


class TestLogin(unittest.TestCase):
    def setUp(self):
        self.login_api = LoginIHRM()

    def test01_login(self):
        response = self.login_api.login({"mobile": "13800000002", "password": "123456"})
        print(response.json())
        common_assert(self, response, 200, True, 10000, "操作成功")

        # 提取TOKEN信息
        config.TOKEN = "Bearer " + response.json().get("data")
        print(config.TOKEN)
        config.headers_data['Authorization'] = config.TOKEN
        print(config.headers_data['Authorization'])

