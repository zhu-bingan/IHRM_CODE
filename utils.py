# _*_ coding: UTF-8 _*_
# 开发者：朱炳安
# 开发人员：ZHUBINGAN
# 开发时间：2021/9/24 21:54
# 文件名称：utils.PY
# 开发工具：PyCharm

# 公共断言方法
def common_assert(case, response, status_code, success, code, message):
    case.assertEqual(status_code, response.status_code)
    case.assertEqual(success, response.json().get("success"))
    case.assertEqual(code, response.json().get("code"))
    case.assertIn(message, response.json().get("message"))