# _*_ coding: UTF-8 _*_
# 开发者：朱炳安
# 开发人员：ZHUBINGAN
# 开发时间：2021/9/24 21:55
# 文件名称：config.PY
# 开发工具：PyCharm

import os

# 项目根目录
BASE_DIR = os.path.dirname(os.path.abspath(__file__))

TOKEN = None

# 项目基础URL
BASE_URL = "http://ihrm-test.itheima.net"

# 请求头数据
headers_data = {
    "Content-Type": "application/json",
    "Authorization": "Bearer 9ad35219-2693-4794-9277-a6a553ce639f"
}