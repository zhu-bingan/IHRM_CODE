# _*_ coding: UTF-8 _*_
# 开发者：朱炳安
# 开发人员：ZHUBINGAN
# 开发时间：2021/9/25 19:26
# 文件名称：employee.PY
# 开发工具：PyCharm


"""
添加员工： /api/sys/user
修改员工
查询员工
删除员工
"""
import requests
import config


class EmployeeManage:
    # 初始化
    def __init__(self):
        self.url_add_employee = config.BASE_URL + "/api/sys/user"
        self.url_update_employee = config.BASE_URL + "/api/sys/user/{}"
        self.url_get_employee = config.BASE_URL + "/api/sys/user/{}"
        self.url_delete_employee = config.BASE_URL + "/api/sys/user/{}"

    # 添加员工
    def add_employee(self, add_employee_data):
        return requests.post(url=self.url_add_employee, json=add_employee_data, headers=config.headers_data)

    # 修改员工
    def update_employee(self, employee_id, update_data):
        url = self.url_update_employee.format(employee_id)
        return requests.put(url=url, json=update_data, headers=config.headers_data)

    # 查询员工
    def get_employee(self, employee_id):
        url = self.url_get_employee.format(employee_id)
        return requests.get(url=url, headers=config.headers_data)

    def del_employee(self, employee_id):
        url = self.url_get_employee.format(employee_id)
        return requests.delete(url=url, headers=config.headers_data)