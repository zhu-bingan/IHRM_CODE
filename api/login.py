# _*_ coding: UTF-8 _*_
# 开发者：朱炳安
# 开发人员：ZHUBINGAN
# 开发时间：2021/9/24 21:57
# 文件名称：IRHM——login.PY
# 开发工具：PyCharm

import requests


class LoginIHRM:
    def __init__(self):
        self.url = "http://ihrm-test.itheima.net/api/sys/login"

    def login(self, login_data):
        return requests.post(url=self.url, json=login_data)


