# _*_ coding: UTF-8 _*_
# 开发者：朱炳安
# 开发人员：ZHUBINGAN
# 开发时间：2021/9/24 21:55
# 文件名称：run_suite.PY
# 开发工具：PyCharm

import time
import unittest
from scripts.test01_login_v1 import TestLogin
from tools.HTMLTestRunner import HTMLTestRunner
from scripts.test03_employee import TestEmployee

suite = unittest.TestSuite()
# 登录接口测试用例
suite.addTest(unittest.makeSuite(TestLogin))
# 员工管理场景接口测试用例
suite.addTest(TestLogin("test01_login"))
suite.addTest(unittest.makeSuite(TestEmployee))

report = "./test_report/report.html"
# report = "./test_report/report-{}.html".format(time.strftime("%Y%m%d-%H%M%S"))


with open(report, "wb") as f:
    runner = HTMLTestRunner(f, title="API Report")
    runner.run(suite)
